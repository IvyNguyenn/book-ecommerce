const path = require("path");
const HtmlWebpackPlugin = require("html-webpack-plugin");
const SRC_DIR = path.resolve(__dirname, "src");
const OUTPUT_DIR = path.resolve(__dirname, "build");
const Dotenv = require("dotenv-webpack");

const ASSETS_DIR = SRC_DIR + "/assets";
const COMPONENTS_DIR = SRC_DIR + "/components";
const UTILS_DIR = SRC_DIR + "/utils";
const SERVICES_DIR = SRC_DIR + "/services";
const CONSTANTS_DIR = SRC_DIR + "/constants";
const LAYOUT_DIR = SRC_DIR + "/layouts";
const UI_KITS_DIR = SRC_DIR + "/components/ui-kits";
const webpack = require("webpack");
const devServer = {
    port: 4000,
    open: true,
    disableHostCheck: true,
    historyApiFallback: true,
    overlay: true,
    stats: "minimal",
    inline: true,
    compress: true,
    contentBase: "/",
};
module.exports = {
    entry: {
        bundle: ["babel-polyfill", "./src/index.js"],
    },
    output: {
        path: OUTPUT_DIR,
        filename: "[name].js",
        publicPath: "/",
    },
    resolve: {
        extensions: [".ts", ".tsx", ".js", ".json"],
        alias: {
            assets: ASSETS_DIR,
            components: COMPONENTS_DIR,
            utils: UTILS_DIR,
            services: SERVICES_DIR,
            constants: CONSTANTS_DIR,
            layouts: LAYOUT_DIR,
            uikits: UI_KITS_DIR,
        },
    },

    module: {
        rules: [
            {
                use: ["babel-loader", "eslint-loader"],
                test: /\.js$/,
                exclude: /node_modules/,
            },
            {
                use: ["style-loader", "css-loader", "sass-loader"],
                test: /\.(css|sass|scss)$/,
            },
            {
                loader: "file-loader",
                test: /\.jpg$|\.gif$|\.png$|\.svg$|\.svg$|\.wav$\.mp3$\.ico$/,
                options: {
                    name: "images/[name].[ext]",
                    //   publicPath: 'images/',
                    //   outputPath: 'images/'
                },
            },
            {
                test: /\.(ttf|eot|woff(2)?)(\S+)?$/,
                loader: "file-loader",
                options: {
                    limit: 1024,
                    name: "[name].[ext]",
                    publicPath: "fonts/",
                    outputPath: "fonts/",
                },
            },
        ],
    },
    plugins: [
        new HtmlWebpackPlugin({
            inlineManifestWebpackName: "webpackManifest",
            title: "Our Site",
            template: "src/index.html",
        }),
        new webpack.ProvidePlugin({
            React: "react",
        }),
        new Dotenv({
            path: "./config.env",
            safe: true,
            systemvars: true, // load all the predefined 'process.env' variables which will trump anything local per dotenv specs.
            silent: true, // hide any errors
            defaults: false,
        }),
    ],
    devServer,
};
