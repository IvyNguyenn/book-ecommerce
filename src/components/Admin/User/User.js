import React, { Component } from "react";
import { Table, Tooltip, Button, Modal, Avatar } from "antd";
import { getUserInfo } from "services/UserService";
import localStorageService from "services/LocalStorageService";

const { Column } = Table;

class User extends Component {
    constructor(props) {
        super(props);
        this.state = {
            users: [],
            isFetching: false,
        };
    }

    fetchUser = () => {
        this.setState({ isFetching: true });
        const { userQuantity } = this.state;
        getUserInfo(userQuantity)
            .then(data => {
                localStorageService.set("USERS", data.results);
                this.setState({
                    users: data.results,
                    isFetching: false,
                });
            })
            .catch(e => console.log(e));
    };

    componentDidMount() {
        this.fetchUser();
    }

    render() {
        const { users, isFetching } = this.state;
        const dataSource =
            users &&
            users.map((item, index) => ({
                image: (
                    <div
                        style={{
                            height: 48,
                            width: 48,
                            textAlign: "center",
                        }}
                    >
                        <Avatar
                            src={item.picture.thumbnail}
                            alt={item.email}
                            shape="square"
                            size="large"
                            icon="warning"
                        />
                    </div>
                ),
                name: item.name.last,
                email: item.email,
                phone: item.phone,
                key: index,
                button: [
                    { title: "Edit", icon: "edit" },
                    {
                        title: "Delete",
                        icon: "delete",
                        // onClick: () => this.showDeleteConfirm(item.id, item.slug, deleteBrandRequest),
                    },
                ],
            }));
        return (
            <Table dataSource={dataSource} loading={isFetching}>
                <Column title="Image" dataIndex="image" key="image" />
                <Column title="Name" dataIndex="name" key="name" />
                <Column title="Email" dataIndex="email" key="email" />
                <Column title="Phone" dataIndex="phone" key="phone" />
                <Column
                    dataIndex="button"
                    key="button"
                    render={buttons => (
                        <span style={{ float: "right" }}>
                            {buttons.map((button, index) => (
                                <Tooltip placement="top" title={button.title} key={index}>
                                    <Button
                                        type="primary"
                                        shape="circle"
                                        icon={button.icon}
                                        onClick={button.onClick}
                                        style={{ marginLeft: 10 }}
                                    />
                                </Tooltip>
                            ))}
                        </span>
                    )}
                />
            </Table>
        );
    }
}

export default User;
