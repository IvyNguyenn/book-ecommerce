export { default as Login } from "./Login/Login";
export { default as Home } from "./Home/Home";
export { default as Profile } from "./Profile/Profile";
export { default as PageNotFound } from "./Error/PageNotFound/PageNotFound";

//Admin
export { default as Dashbroad } from "./Admin/Dashboard/Dashbroad";
export { default as AdminUser } from "./Admin/User/User";
