import React, { Component } from "react";
import PropTypes from "prop-types";
import "./Login.scss";
import { Redirect, Link } from "react-router-dom";
import { Card, Form, Icon, Input, Button } from "antd";

import { LOGIN_LOCALSTORAGE_OBJECT, STRING, INFORM, URL_DEFINE } from "constants/Const";
import Inform from "layouts/Inform/Inform";
import localStorageService from "services/localStorageService";
import { postLogin } from "services/LoginService";
import { FORM_ERROR_MESSAGE } from "constants/Message";

class Login extends Component {
    constructor(props) {
        super(props);

        this.state = {
            email: STRING.EMPTY,
            password: STRING.EMPTY,
            error: STRING.EMPTY,
            validated: false,
            isLoggedin: false,
            isAuthenticated: localStorageService.get(LOGIN_LOCALSTORAGE_OBJECT.TOKEN_TIME_OUT) > new Date().getTime(),
        };
        this.handelLogin = this.handelLogin.bind(this);
        this.handleChange = this.handleChange.bind(this);
    }

    /**
     * Handle Login
     */
    handelLogin = e => {
        const form = e.currentTarget;
        e.preventDefault();
        if (form.checkValidity() === false) {
            e.stopPropagation();
            this.setState({ validated: true, error: "Form is not valid" });
            return;
        }
        const { email, password } = this.state;
        postLogin(email, password)
            .then(res => {
                console.log({ res });
            })
            .catch(e => console.log(e));
        localStorage.setItem(LOGIN_LOCALSTORAGE_OBJECT.LOGIN_SAVE_USE_NAME, email);
        this.setState({ isLoggedin: true, validated: true });
    };

    /**
     * Handle change value from FORM
     * @param event
     */
    handleChange(event) {
        const name = event.target.name;
        const value = event.target.value;
        this.setState({
            [name]: value,
            error: STRING.EMPTY,
        });
    }

    render() {
        const { getFieldDecorator } = this.props.form;
        const { error, isLoggedin } = this.state;
        if (isLoggedin) return <Redirect to={{ pathname: URL_DEFINE.Home }} />;
        return (
            <div className="login-box">
                <Card>
                    <h2 className="title-center">Login</h2>
                    <Form onSubmit={this.handelLogin} className="login-form" noValidate>
                        <Form.Item hasFeedback>
                            {getFieldDecorator("email", {
                                rules: [{ required: true, message: FORM_ERROR_MESSAGE.Email }],
                            })(
                                <Input
                                    prefix={<Icon type="user" style={{ color: "rgba(0,0,0,.25)" }} />}
                                    placeholder="Email"
                                    type="email"
                                    name="email"
                                    onChange={this.handleChange}
                                />
                            )}
                        </Form.Item>
                        <Form.Item hasFeedback>
                            {getFieldDecorator("password", {
                                rules: [{ required: true, message: FORM_ERROR_MESSAGE.Password }],
                            })(
                                <Input
                                    prefix={<Icon type="user" style={{ color: "rgba(0,0,0,.25)" }} />}
                                    placeholder="Password"
                                    type="password"
                                    name="password"
                                    onChange={this.handleChange}
                                />
                            )}
                        </Form.Item>
                        <Form.Item>
                            <Button type="primary" block htmlType="submit">
                                Login
                            </Button>
                        </Form.Item>
                        <div className="title-center">
                            <Link to="#">Forgot password?</Link>
                        </div>
                    </Form>
                </Card>
                <Inform type={INFORM.FAIL} messages={error} />
            </div>
        );
    }
}

Login.propTypes = {
    form: PropTypes.instanceOf(Object),
};

export default Form.create({ name: "normal_login" })(Login);
