import React, { Component } from "react";

class PageNotFound extends Component {
    render() {
        return (
            <div>
                <h5>404 Page Not Found</h5>
            </div>
        );
    }
}

export default PageNotFound;
