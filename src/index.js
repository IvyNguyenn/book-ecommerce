import React from "react";
import ReactDOM from "react-dom";
import App from "./App";
import "antd/dist/antd.css";
import "font-awesome/css/font-awesome.min.css";
// import "roboto-fontface/css/roboto/sass/roboto-fontface.scss";
// import "roboto-fontface/css/roboto-condensed/sass/roboto-condensed-fontface.scss";
// import "roboto-fontface/css/roboto-slab/sass/roboto-slab-fontface.scss";

ReactDOM.render(<App />, document.getElementById("root"));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: http://bit.ly/CRA-PWA
// serviceWorker.unregister();
