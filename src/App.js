import React, { Component } from "react";
import { Header, Inform, AdminLayout, BasicLayout } from "layouts";
import { BrowserRouter as Router, HashRouter, Route, Switch } from "react-router-dom";
import Login from "./components/Login/Login";
import PageNotFound from "./components/Error/PageNotFound/PageNotFound";
import { setService } from "services/Service.config";
import { PrivateRoute } from "./route/PrivateRoute";
import { routes } from "./route/Route";
import { INFORM, ENV, URL_DEFINE, STRING, ROLES } from "constants/Const";

export const InformContext = React.createContext();

class App extends Component {
    constructor(props) {
        super(props);
        this.state = {
            messageInform: STRING.EMPTY,
            isSuccess: false,
            errorCode: 0,
        };
    }

    shouldComponentUpdate(newProps, newState) {
        return this.state.messageInform != newState.messageInform;
    }

    componentDidUpdate() {
        this.setState({ messageInform: STRING.EMPTY });
    }

    /**
     * Handle Inform Error/Success Message
     */
    handleInform = (messages, isSuccess, errorCode) => {
        this.setState({ messageInform: messages, isSuccess: isSuccess, errorCode: errorCode });
    };

    renderLayout = (route, props) => {
        if (route.acceptedRoles) {
            return <AdminLayout {...route} {...props} {...this.props} />;
        }
        return <BasicLayout {...route} {...props} {...this.props} />;
    };

    renderRoute = () =>
        routes.map((route, index) => (
            <PrivateRoute
                key={index}
                path={route.path}
                exact={route.exact}
                Component={props => this.renderLayout(route, props)}
            />
        ));

    render() {
        const mode = process.env.mode;
        setService(mode);
        const contextValue = {
            handleInform: this.handleInform,
        };

        return (
            <>
                <Router>
                    <Switch>
                        <Route path={URL_DEFINE.Login} history={history} component={Login} />
                        {this.renderRoute()}
                    </Switch>
                </Router>
                <Inform
                    type={this.state.isSuccess ? INFORM.SUCCESS : INFORM.FAIL}
                    messages={this.state.messageInform}
                />
            </>
        );
    }
}

export default App;
