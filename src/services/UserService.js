import { API_GET_USER_INFO } from "constants/APINames";
import { METHOD, TYPE_REQUEST } from "constants/Const";
import { service } from "./Service.config";

export function getUserInfo(quantity = 4) {
    return service
        .sendRequest(TYPE_REQUEST.AUTHENTICATION, METHOD.GET, `${API_GET_USER_INFO}${quantity}`)
        .then(res => res.data);
}
