import axios from "axios";
import { ENV, TIME_OUT, LOGIN_LOCALSTORAGE_OBJECT, TYPE_REQUEST, STRING } from 'constants/Const'
import { ERROR_MESSAGE } from "constants/Message"
import localStorageService from "services/localStorageService"

let BASE_URL = STRING.EMPTY;

/**
 * Set environment
 * @param env : Environment name
 */
function setService(env) {
    if (env === ENV.DEV) {
        BASE_URL = process.env.BASE_URL_DEV;
    } else if (env === ENV.PRO) {
        BASE_URL = process.env.BASE_URL_PRO;
    } else {
        BASE_URL = STRING.EMPTY
    }
}

class Network {
    static instance = new Network();

    token = STRING.EMPTY;
    constructor() {
        if (Network.instance) {
            throw new Error(
                ERROR_MESSAGE.INSTANTIATION_FAIL.MESSAGE
            );
        }
        Network.instance = this;
    }
    static getInstance() {
        return Network.instance;
    }

    setToken(token) {
        this.token = token;
    }

    getToken() {
        if (!this.token) {
            this.token = localStorageService.get(LOGIN_LOCALSTORAGE_OBJECT.TOKEN)
        }
        return this.token;
    }

    /**
     * sendRequest
     * @param method : GET / POST / PUT / DELETE
     * @param endpoint : URL
     * @param body : JSON object
     * @returns {Promise<AxiosResponse<any> | never>}
     */
    sendRequest(type, method, url, data = {}, header = STRING.EMPTY) {
        header = Object.assign(
            header, type === TYPE_REQUEST.AUTHENTICATION ? { "X-Token": this.getToken() } : STRING.EMPTY,
            header["Content-Type"] ?"":{"Content-Type": "application/json"}
        );
        return axios({
            method: method,
            url: url,
            baseURL: BASE_URL,
            data: data,
            timeout: TIME_OUT.INTERVAL_REQUEST_SERVER,
            headers: {
                ...header,
                "Access-Control-Allow-Origin": '*',
                version: "",
                pkcs: "15",
            }
        }).then(res => {
            if (res.data.errorCode && res.data.errorCode == ERROR_MESSAGE.AUTHEN.CODE) {
                localStorageService.removeAll();
                return res;
            } else {
                return res;
            }
        })
    }
}

const service = Network.getInstance();
export { setService, service };
