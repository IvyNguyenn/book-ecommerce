import { service } from "./Service.config"
import { METHOD, TYPE_REQUEST } from "constants/Const"
import { API_LOGIN, API_PUBLIC_KEY, API_LOGOUT, API_CHANGE_PASSWORD } from "constants/APINames"

/**
 * Post User Login
 * @returns {Promise<AxiosResponse<any> | never | never>}
 */
export function postLogin(userName, password) {
    return service.sendRequest(TYPE_REQUEST.UNAUTHENTICATION, METHOD.POST, API_LOGIN, { userName, password })
        .then(res => res.data);
}

/**
 * Post User Login
 * @returns {Promise<AxiosResponse<any> | never | never>}
 */
export function postLogout() {
    return service.sendRequest(TYPE_REQUEST.AUTHENTICATION, METHOD.POST, API_LOGOUT)
        .then(res => res.data);
}

/**
 * Change password
 * @returns {Promise<AxiosResponse<any> | never | never>}
 */
export function postChangePassword(currentPassword, newPassword) {
    return service.sendRequest(TYPE_REQUEST.AUTHENTICATION, METHOD.POST,
        API_CHANGE_PASSWORD, { currentPassword, newPassword })
        .then(res => res.data);
}
