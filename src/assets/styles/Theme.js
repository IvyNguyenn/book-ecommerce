import "./Theme.scss"

export const COLOR = {
    Green1: "#3c8dbc",
    // Green1: "#7BBB49",
    Green2: "#4bd865",
    Black: "#2a2e43",
    White: "#fff",
    Milk: "#d8d9db",
    Organ: "#fba442",
    Red: "#d0021b"
};
