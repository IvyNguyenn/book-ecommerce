const images = {
    manUser: require("./images/man-user.svg"),
    password: require("./images/password.svg"),
};

export default {
    images,
};
