import React, { Component } from 'react'
import 'react-notifications/lib/notifications.css'
import { NotificationContainer, NotificationManager } from 'react-notifications'
import {INFORM} from "constants/Const"
import "./Inform.scss"
import PropTypes from 'prop-types'

class Inform extends Component {

  createNotification = type => {
      switch (type) {
        case INFORM.FAIL:
          NotificationManager.error(this.props.messages, type);
          break;
        case INFORM.SUCCESS:
          NotificationManager.success( this.props.messages, type);
          break;
        case INFORM.WARNING:
          NotificationManager.warning(this.props.messages, type);
          break;
        default:
          break;
      }
  }

  render() {
    return (
      <div className="page-inform">
        { (typeof this.props.messages !== 'undefined') && (this.props.messages) &&
          this.createNotification(this.props.type)}
        <NotificationContainer />
      </div>
    )
  }
}

Inform.propTypes ={
    messages: PropTypes.string.isRequired,
    type :  PropTypes.string.isRequired
}
export default Inform
