import React, { Component } from "react";
import { Layout, Menu, Breadcrumb, Icon } from "antd";
import { Link } from "react-router-dom";
import { MENU_ADMIN } from "constants/Const";
const { Header, Content, Footer, Sider } = Layout;
const SubMenu = Menu.SubMenu;

class AdminLayout extends Component {
    constructor(props) {
        super(props);
        this.state = {
            collapsed: false,
        };
    }

    onCollapse = collapsed => {
        this.setState({ collapsed });
    };

    renderMenuBar = menus =>
        menus.map(menu => {
            if (menu.children && menu.children.length > 0) {
                return (
                    <SubMenu
                        key={menu.id}
                        title={
                            <span>
                                <Icon type={menu.icon} />
                                <span>{menu.label}</span>
                            </span>
                        }
                    >
                        {menu.children.map(children => (
                            <Menu.Item key={children.id}>
                                <Link to={children.to}>
                                    <Icon type={children.icon} />
                                    <span>{children.label}</span>
                                </Link>
                            </Menu.Item>
                        ))}
                    </SubMenu>
                );
            } else {
                return (
                    <Menu.Item key={menu.id}>
                        <Link to={menu.to}>
                            <Icon type={menu.icon} />
                            <span>{menu.label}</span>
                        </Link>
                    </Menu.Item>
                );
            }
        });

    render() {
        const { main: Component } = this.props;
        return (
            <Layout style={{ minHeight: "100vh" }}>
                <Sider collapsible collapsed={this.state.collapsed} onCollapse={this.onCollapse}>
                    <div className="logo" style={{ color: "#fff" }}>
                        Logo
                    </div>
                    <Menu theme="dark" defaultSelectedKeys={["0"]} mode="inline">
                        {this.renderMenuBar(MENU_ADMIN)}
                    </Menu>
                </Sider>
                <Layout>
                    <Header style={{ background: "#fff", padding: 0 }} />
                    <Content style={{ margin: "0 16px" }}>
                        <Breadcrumb style={{ margin: "16px 0" }}>
                            <Breadcrumb.Item>User</Breadcrumb.Item>
                            <Breadcrumb.Item>Bill</Breadcrumb.Item>
                        </Breadcrumb>
                        <div style={{ background: "#fff" }}>
                            <Component />
                        </div>
                    </Content>
                    <Footer style={{ textAlign: "center" }}>Ant Design ©2018 Created by Ant UED</Footer>
                </Layout>
            </Layout>
        );
    }
}

export default AdminLayout;
