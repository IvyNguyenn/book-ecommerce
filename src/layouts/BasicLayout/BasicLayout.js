import React, { Component } from "react";
import { Layout, Menu, Breadcrumb, Icon } from "antd";
import { Link } from "react-router-dom";
import { MENU_ADMIN } from "constants/Const";
const { Header, Content, Footer } = Layout;

class BasicLayout extends Component {
    constructor(props) {
        super(props);
        this.state = {
            collapsed: false,
        };
    }

    render() {
        const { main: Component } = this.props;
        return (
            <Layout>
                <Header style={{ background: "#fff", padding: 0 }} />
                <Content style={{ margin: "0 16px" }}>
                    <div style={{ background: "#fff" }}>
                        <Component />
                    </div>
                </Content>
                <Footer style={{ textAlign: "center" }}>Ant Design ©2018 Created by Ant UED</Footer>
            </Layout>
        );
    }
}

export default BasicLayout;
