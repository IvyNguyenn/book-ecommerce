export { default as Inform } from "./Inform/Inform";
export { default as Header } from "./Header/Header";
export { default as AdminLayout } from "./AdminLayout/AdminLayout";
export { default as BasicLayout } from "./BasicLayout/BasicLayout";
