import React, { Component } from "react";
import "./Header.scss";
import { LOGIN_LOCALSTORAGE_OBJECT, STRING, TIME_OUT, URL_DEFINE } from "constants/Const";
import { Redirect } from "react-router-dom";
import { postLogout } from "services/LoginService";
import localStorageService from "services/localStorageService";
import PropTypes from "prop-types";

class Header extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isLogout: false,
        };
    }

    handleLogout = () => {
        postLogout()
            .then(() => {
                localStorageService.removeAll();
                this.setState({ isLogout: true });
            })
            .catch(error => {
                this.setState(prevState => (prevState.error = error));
            });
        localStorageService.removeAll();
        this.setState({ isLogout: true });
    };

    render() {
        const { isLogout } = this.state;
        return (
            <div>
                {isLogout ? <Redirect to={URL_DEFINE.Login} /> : STRING.EMPTY}
                <div className="bar-responsive">
                    <aside className="logo" />
                    <aside className="main-header">
                        <i className="fa fa-sign-out" onClick={this.handleLogout} />
                    </aside>
                </div>
            </div>
        );
    }
}

Header.propTypes = {
    ToggleMenuClick: PropTypes.func,
};

export default Header;
