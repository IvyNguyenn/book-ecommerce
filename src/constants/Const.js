import assets from "assets";

// Method call API
export const METHOD = {
    GET: "GET",
    POST: "POST",
    PUT: "PUT",
    DELETE: "DELETE",
};

export const RADIAN = Math.PI / 180;

export const COLORS_PIECHART = ["#0088FE", "#00C49F", "#FFBB28", "#FF8042"];
export const STRING = {
    EMPTY: "",
    DOT: ".",
    COMMA: ",",
    QUESTION_MARK: "?",
};

export const OBJECT_TYPE = {
    NUMBER: "number",
};

// Localstorage
export const LOGIN_LOCALSTORAGE_OBJECT = {
    LOGIN_SAVE_USE_NAME: "userName",
    TOKEN_TIME_OUT: "timeout",
    TOKEN: "token",
    PUBLICKEY: "PUBLICKEY",
};

export const POSITION = {
    FIRST: 0,
    SECOND: 1,
    THIRST: 2,
};

export const ACTIVE = {
    ON: {
        NAME: "ON",
        VALUE: 1,
        NAME_DIFF: "ACTIVE",
    },
    OFF: {
        NAME: "OFF",
        VALUE: 0,
        NAME_DIFF: "DISABLE",
    },
};

export const TIME_OUT = {
    INTERVAL_CHECK_TOKEN: 1000,
    TOKEN_TIME_OUT: 1000,
    TIMEOUT_SLEEP_API: 500,
};

export const FORTMAT_DATE = {
    TIME_BEGIN_DAY: " 00:00:00",
    TIME_END_DAY: " 23:59:59",
    DATE: "DD/MM/YYYY",
    DATE_NO_SLASH: "YYYYMMDD",
    DATE_TIME: "YYYY-MM-DD HH:mm:ss",
};

export const INFORM = {
    FAIL: "Error",
    WARNING: "Warning",
    SUCCESS: "Success",
};

export const ENV = {
    DEV: "DEV",
    PRO: "PRO",
};

export const SORT_TYPE = {
    ASC: "asc",
    DESC: "desc",
};

export const URL_DEFINE = {
    ID: ":id",
    Home: "/",
    Login: "/login",

    //Admin
    Dashboard: "/admin",
    Users: "/admin/user",
    Products: {
        Product: "/admin/product",
        Category: "/admin/product/category",
    },
    Orders: "order",

    OtherURL: "/*",
};

export const TYPE_REQUEST = {
    AUTHENTICATION: "AUTHENTICATION",
    UNAUTHENTICATION: "UNAUTHENTICATION",
};

export const ROLES = {
    ADMIN: "ADMIN",
};

/* Menu Define*/

export const MENU_ADMIN = [
    {
        id: 10,
        label: "Dashboard",
        to: URL_DEFINE.Dashboard,
        exact: true,
        icon: "dashboard",
    },
    {
        id: 20,
        label: "Users",
        to: URL_DEFINE.Users,
        exact: true,
        icon: "user",
    },
    {
        id: 30,
        label: "Products",
        to: URL_DEFINE.Products.Product,
        exact: true,
        icon: "shopping",
        children: [
            {
                id: 31,
                label: "Product",
                to: URL_DEFINE.Products.Product,
                exact: true,
                icon: "book",
            },
            {
                id: 32,
                label: "Category",
                to: URL_DEFINE.Products.Category,
                exact: true,
                icon: "gold",
            },
        ],
    },
];
