export const ERROR_MESSAGE = {
    AUTHEN: {
        CODE: 403,
        MESSAGE: "",
    },
    WRONG_PASSWORD: {
        CODE: 0,
        MESSAGE: "Please check password",
    },
};

export const FORM_ERROR_MESSAGE = {
    Description: "Please input description",
    DeviceId: "Please input device Id",
    Name: "Please input name",
    Hysteresis: "Please input hysteresis",
    Phone: "Please input phone",
    Email: "Please input email",
    Address: "Please input address",
    UserName: "Please input user name",
    FullName: "Please input full name",
    PhoneNumber: "Please input phone number",
    Password: "Please input password",
    ConfirmPassword: "Please input confirm password",
    CurrentPassword: "Please input current password",
};

export const FORM_MESSAGE = {
    Validated: "look goods",
};

export const INFORM_MESSAGE = {
    Delete: "Are you delete ?",
    RemoveAssign: "Are you remove a terminal assignment: ",
};
