import React from "react";
import { Route, Redirect } from "react-router-dom";
import { LOGIN_LOCALSTORAGE_OBJECT, URL_DEFINE } from "constants/Const";
import localStorageService from "services/localStorageService";
import PropTypes from "prop-types";

export const PrivateRoute = ({ Component, ...rest }) => (
    <Route
        {...rest}
        render={props =>
            localStorageService.get(LOGIN_LOCALSTORAGE_OBJECT.LOGIN_SAVE_USE_NAME) ? (
                <Component {...props} />
            ) : (
                <Redirect to={{ pathname: URL_DEFINE.Login, state: { from: props.location } }} />
            )
        }
    />
);

PrivateRoute.propTypes = {
    Component: PropTypes.func.isRequired,
};
