import * as component from "components";
import { URL_DEFINE, ROLES } from "constants/Const";
import AdminDashbroad from "../components/Admin/Dashboard/Dashbroad";
import AdminUser from "../components/Admin/User/User";

/* Route Define */
export const routes = [
    {
        path: URL_DEFINE.Home,
        exact: true,
        main: () => <component.Home />,
    },

    // Admin
    {
        path: URL_DEFINE.Dashboard,
        exact: true,
        acceptedRoles: [ROLES.ADMIN],
        main: () => <AdminDashbroad />,
    },
    {
        path: URL_DEFINE.User,
        exact: true,
        acceptedRoles: [ROLES.ADMIN],
        main: () => <AdminUser />,
    },
    // Login
    {
        path: URL_DEFINE.Login,
        exact: true,
        main: ({ match, history }) => <component.Login match={match} history={history} />,
    },
    // Other URL
    // {
    //     path: URL_DEFINE.OtherURL,
    //     exact: false,
    //     main: () => <component.PageNotFound />,
    // },
];
